using LiteNetLib.Utils;

namespace LiteNetLibSimpleShared
{
    public struct Point : INetSerializable
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Point(float x, float y)
        {
            X = x;
            Y = y;
        }

        public void Serialize(NetDataWriter writer)
        {
           writer.Put(X);
           writer.Put(Y);
        }

        public void Deserialize(NetDataReader reader)
        {
            X = reader.GetFloat();
            Y = reader.GetFloat();
        }
    }
}