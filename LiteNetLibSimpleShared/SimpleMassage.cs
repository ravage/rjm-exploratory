﻿using System;

namespace LiteNetLibSimpleShared
{
    public class SimpleMessage
    {
        public byte Command { get; set; }
        public string Message { get; set; }

        public Point Point { get; set; }

        public SimpleMessage() {}

        public SimpleMessage(byte command, string message)
        {
            Command = command;
            Message = message;
        }
    }
}