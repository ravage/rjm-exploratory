using LiteNetLib;
using LiteNetLib.Utils;
using LiteNetLibSimpleShared;
using Serilog;

namespace LiteNetLibSimpleClient
{
    public class CustomPacketProcessor : IPacketProcessor
    {
        private readonly NetPacketProcessor processor;

        public CustomPacketProcessor()
        {
            processor = new NetPacketProcessor();
            processor.RegisterNestedType<Point>();
            processor.SubscribeReusable<SimpleMessage>(onReceive);
        }
        
        private void onReceive(SimpleMessage payload)
        {
            Log.Information("Response: {Payload}", payload);
        }

        public void ReadAllPackets(NetDataReader reader)
        {
            processor.ReadAllPackets(reader);
        }

        public void Send<T>(NetPeer peer, T packet, DeliveryMethod deliveryMethod) where T: class, new()
        {
            processor.Send(peer, packet, deliveryMethod);
        }
    }
}