using LiteNetLib;
using LiteNetLib.Utils;
using LiteNetLibSimpleShared;

namespace LiteNetLibSimpleClient
{
    public interface IPacketProcessor
    {
        void ReadAllPackets(NetDataReader reader);
        void Send<T>(NetPeer peer, T packet, DeliveryMethod deliveryMethod) where T : class, new();
    }
}