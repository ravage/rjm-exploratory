using System.Net;
using System.Net.Sockets;
using LiteNetLib;
using LiteNetLib.Utils;
using LiteNetLibSimpleShared;
using Serilog;

namespace LiteNetLibSimpleClient
{
    public class CustomListener : INetEventListener
    {
        private CustomPacketProcessor processor;
        public CustomListener(CustomPacketProcessor processor)
        {
            this.processor = processor;
        }

        public void OnPeerConnected(NetPeer peer)
        {
            //NetDataWriter writer = new NetDataWriter();
            Log.Information("Connected to: {Endpoint}", peer.EndPoint);
            //writer.Put("Thank You!");
            //peer.Send(writer, DeliveryMethod.Unreliable);
            SimpleMessage message = new SimpleMessage();
            message.Command = 0x15;
            message.Message = "Thank You!";
            message.Point = new Point(1f, 5f);
            processor.Send(peer, message, DeliveryMethod.Unreliable);
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
 
        }

        public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
        {
            // throw new System.NotImplementedException();
        }

        public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            processor.ReadAllPackets(reader);
        }

        public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
        {
            // throw new System.NotImplementedException();
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
           // throw new System.NotImplementedException();
        }

        public void OnConnectionRequest(ConnectionRequest request)
        {
            // throw new System.NotImplementedException();
        }
    }
}