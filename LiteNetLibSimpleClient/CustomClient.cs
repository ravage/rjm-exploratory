using System;
using System.Net;
using System.Text;
using System.Threading;
using LiteNetLib;
using LiteNetLib.Utils;
using Serilog;

namespace LiteNetLibSimpleClient
{
    public class CustomClient
    {
        private NetManager client;
        private IPacketProcessor processor;
        
        public CustomClient(INetEventListener listener, IPacketProcessor processor)
        {
            this.processor = processor;
            client = new NetManager(listener);
        }

        public void Connect(string server, int port, string key = "")
        {
            client.Start();
            Log.Information("Connecting to {Server}:{Port}", server, port);
            client.Connect(server, port, key);
            
        }

        public void PoolEvents()
        {
            client.PollEvents();
        }

        public NetManager Socket => client;
    }
}