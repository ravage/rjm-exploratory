﻿using System;
using System.Net;
using System.Threading;
using LiteNetLib.Utils;
using Serilog;

namespace LiteNetLibSimpleClient
{
    class Program
    {
        private static CustomClient client;
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
            string server = "127.0.0.1";
            int port = 9000;

            CustomPacketProcessor processor = new CustomPacketProcessor();
            CustomListener listener = new CustomListener(processor);
            
            Thread.Sleep(1000);
            
            client = new CustomClient(listener, processor);
            
            NetDataWriter writer = new NetDataWriter();
            writer.Put("Unconnected Message");
            client.Socket.SendUnconnectedMessage(writer, new IPEndPoint(IPAddress.Parse(server), port));
            writer.Reset();
            writer.Put("Anyone Out There!");
            client.Socket.SendBroadcast(writer, port);
            
            client.Connect(server, port);
            
            Update();
        }

        static void Update()
        {
            while (!Console.KeyAvailable)
            {
                client.PoolEvents();
                Thread.Sleep(100);
            }
        }
    }
}