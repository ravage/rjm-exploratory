﻿using System;
using System.Diagnostics;
using System.Management.Instrumentation;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SocketServer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Debug.WriteLine("Debug!");    
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
             EndPoint endpoint = new IPEndPoint(IPAddress.Any, 3000);
             server.Bind(endpoint);
             server.Listen(500);

             while (true)
             {
                 Socket client = await server.AcceptAsync();
                 ClientHandler(client);
    
                 //client.Close();
             }
             
             //Console.ReadLine();
        }

        private static async void ClientHandler(Socket client)
        {
            while (true)
            {
                byte[] buffer = new byte[500];
                ArraySegment<byte> segment = new ArraySegment<byte>(buffer);

                try
                {
                    //int bytesRead = client.Receive(buffer);
                    int bytesRead = await client.ReceiveAsync(segment, SocketFlags.None);
                    // Thread.Sleep(1000);
                    
                    if (bytesRead == 0)
                     {
                         client.Close();
                         break;
                     }

                    string message = Encoding.UTF8.GetString(segment.Array);
                    Console.WriteLine(message);
                    if (!message.Contains("123456"))
                    {
                        client.Send(Encoding.UTF8.GetBytes("Unauthorized\n"));
                        client.Close();
                        break;
                    }

                    Console.WriteLine($"Bytes Received: {bytesRead} | Message: {message}");
                    client.Send(Encoding.UTF8.GetBytes("Welcome!\n"));
                } catch (SocketException ex)
                {
                    Console.WriteLine($"Exception: {ex.Message}");
                    break;
                }
            }
        }
    }
}