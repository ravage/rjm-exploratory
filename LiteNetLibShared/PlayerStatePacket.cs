using LiteNetLib.Utils;

public class PlayerStatePacket: INetSerializable
{
    public int Id { get; set; }
    public float X { get; set; }
    public float Y { get; set; }
    public float Horizontal { get; set; }
    public float Vertical { get; set; }
    public bool Throw { get; set; }
    
    public void Serialize(NetDataWriter writer)
    {
        writer.Put(Id);
        writer.Put(X);
        writer.Put(Y);
        writer.Put(Horizontal);
        writer.Put(Vertical);
        writer.Put(Throw);
    }

    public void Deserialize(NetDataReader reader)
    {
        Id = reader.GetByte();
        X = reader.GetFloat();
        Y = reader.GetFloat();
        Horizontal = reader.GetFloat();
        Vertical = reader.GetFloat();
        Throw = reader.GetBool();
    }
}
