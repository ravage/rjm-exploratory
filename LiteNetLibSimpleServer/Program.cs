﻿using System;
using System.Net;
using System.Threading;
using LiteNetLib;
using LiteNetLib.Utils;
using Serilog;
using LiteNetLibSimpleShared;

namespace LiteNetLibSimpleServer
{
    class Program
    {
        private static NetManager server;
        private static NetPacketProcessor processor;

        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
            EventBasedNetListener listener = new EventBasedNetListener();
            server = new NetManager(listener);
            processor = new NetPacketProcessor();
            server.UnconnectedMessagesEnabled = true;
            server.BroadcastReceiveEnabled = true;
            server.Start(9000);
            
            processor.RegisterNestedType<Point>();
            processor.SubscribeReusable<SimpleMessage, NetPeer>(OnReceive);

            listener.ConnectionRequestEvent += ListenerOnConnectionRequestEvent;
            listener.PeerConnectedEvent += ListenerOnPeerConnectedEvent;
            listener.NetworkReceiveEvent += ListenerOnNetworkReceiveEvent;
            listener.NetworkReceiveUnconnectedEvent += ListenerOnNetworkReceiveUnconnectedEvent;
            listener.PeerDisconnectedEvent += ListenerOnPeerDisconnectedEvent;

            Log.Information("Server Started");
            while (!Console.KeyAvailable)
            {
                server.PollEvents();
                Thread.Sleep(100);
            }
            Log.Information("Server Terminated");
        }

        private static void OnReceive(SimpleMessage payload, NetPeer peer)
        {
            NetDataWriter writer = new NetDataWriter();
            processor.Write(writer, payload);
            Log.Information("{Peer}: {Message}", payload, peer);
            server.SendToAll(writer, DeliveryMethod.Unreliable);
        }

        private static void ListenerOnPeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectinfo)
        {
            Log.Information("Event: OnPeerDisconnectedEvent");
        }

        private static void ListenerOnNetworkReceiveUnconnectedEvent(IPEndPoint remoteendpoint, NetPacketReader reader, UnconnectedMessageType messagetype)
        {
            Log.Information("Event: OnNetworkReceiveUnconnectedEvent");
            Log.Information("Payload: {Payload}", reader.GetString());
        }

        private static void ListenerOnNetworkReceiveEvent(NetPeer peer, NetPacketReader reader, DeliveryMethod deliverymethod)
        {
            //NetDataWriter writer = new NetDataWriter();
            Log.Information("Event: OnNetworkReceiveEvent");
            processor.ReadAllPackets(reader, peer);
            // string payload = reader.GetString();
            // Log.Information("Received: {Payload}", payload);
            //writer.Put($"[{peer.EndPoint}]: {payload}");
            // server.SendToAll(writer, DeliveryMethod.Unreliable);
        }

        private static void ListenerOnPeerConnectedEvent(NetPeer peer)
        {
            Log.Information("Event: OnPeerConnectedEvent");

            foreach (NetPeer client in server.ConnectedPeerList)
            {
                Log.Information("Peer: {Endpoint}", client.EndPoint);
            }
        }

        private static void ListenerOnConnectionRequestEvent(ConnectionRequest request)
        {
            Log.Information("Event: OnConnectionRequestEvent");
            request.Accept();
        }
    }
}