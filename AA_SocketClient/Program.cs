﻿using System;
using System.Net.Sockets;
using System.Text;

namespace SocketClient
{
    class Program
    {
        static void Main(string[] args)
        {   
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client.Connect("localhost", 3000);
            client.Send(Encoding.UTF8.GetBytes("123456"));
        
            while (true)
            {
                byte[] buffer = new byte[500];
                
                try
                {
                    client.Receive(buffer);
                    Console.WriteLine(Encoding.UTF8.GetString(buffer));
                    
                    string input = Console.ReadLine();
                    client.Send(Encoding.UTF8.GetBytes(input));
                }
                catch (SocketException e)
                {
                    Console.WriteLine(e);
                    break;
                }
            }
        }
    }
}