﻿using System;
using System.Threading;
using LiteNetLib;
using Serilog;

namespace LiteNetLibServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Run();
        }

        static void Run()
        {
            Log.Logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
            DemoServer listener = new DemoServer();
            NetManager server = new NetManager(listener);
            server.AutoRecycle = true;
            server.SimulateLatency = false;
            server.Start(3000);
            //server.DiscoveryEnabled = true;
            listener.Server = server;

            Log.Information("Server Started");
            while (!Console.KeyAvailable)
            {
                server.PollEvents();
                Thread.Sleep(15);
            }
            
            server.Stop();
        }
    }
}