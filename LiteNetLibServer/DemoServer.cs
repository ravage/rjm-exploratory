using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using LiteNetLib;
using LiteNetLib.Utils;
using Serilog;

namespace LiteNetLibServer
{
    public class DemoServer : INetEventListener
    {
        public NetManager Server { get; set; }
        private NetPacketProcessor processor;
        private Random random;
        private Dictionary<int, PlayerState> players;

        public DemoServer()
        {
            random = new Random();
            processor = new NetPacketProcessor();
            players = new Dictionary<int, PlayerState>();
            
            processor.RegisterNestedType(() => new PlayerStatePacket());
            processor.RegisterNestedType(() => new PlayerJoinPacket());
            //processor.RegisterNestedType<Point>();
            
            processor.SubscribeReusable<PlayerStatePacket, NetPeer>(PlayerStateHandler);
            processor.SubscribeReusable<ThrowPacket, NetPeer>(ThrowPacketHandler);
            //processor.SubscribeReusable<SimplePacket, NetPeer>(SimplePacketHandler);
            //processor.SubscribeReusable<CommandPacket, NetPeer>(CommandPacketHandler);
            //processor.SubscribeReusable<GameState, NetPeer>(GameStateHandler);
        }

        private void ThrowPacketHandler(ThrowPacket packet, NetPeer sender)
        {
            Log.Information("{Packet}", packet);
            foreach (NetPeer peer in Server.ConnectedPeerList)
            {
                if (sender.Id == peer.Id)
                {
                    continue;
                }
                
                processor.Send(peer, packet, DeliveryMethod.Unreliable);
            }
        }

        private void PlayerStateHandler(PlayerStatePacket state, NetPeer sender)
        {
            PlayerState curentState = players[sender.Id];
            curentState.Update(state);
            Log.Information("{X}, {Y}, {Throw}", state.X, state.Y, state.Throw);
            
            foreach (NetPeer peer in Server.ConnectedPeerList)
            {
                if (sender.Id == peer.Id)
                {
                    continue;
                }
                
                processor.Send(peer, state, DeliveryMethod.Unreliable);
            }
        }

        public void OnPeerConnected(NetPeer peer)
        {
            Log.Information("Peer connected: {Peer}", peer.EndPoint);
            List<NetPeer> peers = Server.ConnectedPeerList;

            PlayerJoinPacket spawn = new PlayerJoinPacket()
            {
                Id = peer.Id,
                Name = "Player #" + random.Next(10, 100),
                X = random.Next(-6, 5),
                Y = random.Next(-4, 4)
            };
            
            GameStatePacket gameState = new GameStatePacket()
            {
                Player = spawn,
                Players = players.Values.ToPlayerJoinPackets()
            };
            
            players.Add(peer.Id, new PlayerState(spawn));
            
            processor.Send(peer, gameState, DeliveryMethod.Unreliable);
            Log.Information("{Peer}, {GameState}", peer, gameState);
            foreach  (NetPeer netPeer in peers)
            {
                if (netPeer.Id == peer.Id)
                {
                    continue;
                }
                
                processor.Send(netPeer, spawn, DeliveryMethod.Unreliable);
                Log.Information("ConnectedPeersList: id={Id}, ep={Peer}", netPeer.Id, netPeer.EndPoint);
            }
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            Log.Information("Client {Id} disconnected: {Reason}", peer.Id, disconnectInfo.Reason);
            players.Remove(peer.Id);
            
            PlayerDisconnectPacket playerDisconnect = new PlayerDisconnectPacket()
            {
                Id = peer.Id
            };
            
            Server.SendToAll(processor.Write(playerDisconnect), DeliveryMethod.Unreliable);
        }

        public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
        {
            Log.Information("Error: {Error}", socketError);
        }

        public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            processor.ReadAllPackets(reader, peer);
        }

        public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
        {
            /*string message = reader.GetString(100);
            Log.Information("Received unconnected message {MessageType} from {RemoteEndPoint}: {Data}", messageType, remoteEndPoint, message);
            NetDataWriter writer = new NetDataWriter();
            
            if (messageType == UnconnectedMessageType.DiscoveryRequest)
            {
                writer.Put("ACK");
            }
            else
            {
                writer.Put("NACK");
            }
            
            Server.SendDiscoveryResponse(writer, remoteEndPoint);
            reader.Recycle();*/
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
        }

        public void OnConnectionRequest(ConnectionRequest request)
        {
            if (request.AcceptIfKey("") == null)
            {
                Log.Information("Connection denied!");
            }
        }
    }
}