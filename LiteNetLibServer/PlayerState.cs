namespace LiteNetLibServer
{
    public class PlayerState
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Horizontal { get; set; }
        public float Vertical { get; set; }
        
        public PlayerState(PlayerJoinPacket player)
        {
            Id = player.Id;
            X = player.X;
            Y = player.Y;
            Name = player.Name;
        }

        public void Update(PlayerStatePacket packet)
        {
            Id = packet.Id;
            X = packet.X;
            Y = packet.Y;
            Horizontal = packet.Horizontal;
            Vertical = packet.Vertical;
        }
    }
}