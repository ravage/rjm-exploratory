using System.Collections.Generic;
using System.Linq;

namespace LiteNetLibServer
{
    public static class Extensions
    {
        public static PlayerJoinPacket[] ToPlayerJoinPackets(this IEnumerable<PlayerState> source)
        {
            int len = source.Count();
            int i = 0;
            PlayerJoinPacket[] packets = new PlayerJoinPacket[len];
            
            foreach (PlayerState state in source)
            {
                packets[i] = new PlayerJoinPacket()
                {
                    Id = state.Id,
                    Name = state.Name,
                    X = state.X,
                    Y = state.Y
                };

                i++;
            }

            return packets;
        }
    }
}