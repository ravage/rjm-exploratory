using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private NetworkManager client;
    private Dictionary<int, PlayerEntity> players;
    private PlayerEntity localPlayer;
    [SerializeField]
    private GameSettings gameSettings;

    private void Start()
    {
        players = new Dictionary<int, PlayerEntity>();
        PacketHandler packetHandler = new PacketHandler();
        packetHandler.OnPlayerState += OnPlayerState;
        packetHandler.OnPlayerDisconnect += OnPlayerDisconnect;
        packetHandler.OnPlayerJoin += OnPlayerJoin;
        packetHandler.OnGameState += OnGameState;
        packetHandler.OnThrowPacket += OnThrowPacket;
        client = new NetworkManager(gameSettings, packetHandler);
        client.Connect();
    }

    public void OnMotion()
    {
        client.Send(localPlayer.State.Packet);
    }
    
    private void OnThrow()
    {
        client.Send(localPlayer.State.ThrowPacket);
    }
    
    private void OnThrowPacket(ThrowPacket packet)
    {
        players[packet.Id].UpdateState(packet);
    }

    private void OnGameState(GameStatePacket packet)
    {
        localPlayer = Instantiate(gameSettings.PlayerPrefab);
        LocalPlayerBehavior behavior = new LocalPlayerBehavior(localPlayer);
        behavior.OnMotion += OnMotion;
        behavior.OnThrow += OnThrow;
        localPlayer.Setup(behavior, packet.Player);
        players.Add(localPlayer.State.Id, localPlayer);
        
        foreach (PlayerJoinPacket state in packet.Players)
        {
           OnPlayerJoin(state);
        }
    }

    private void OnPlayerJoin(PlayerJoinPacket packet)
    {
        PlayerEntity remotePlayer = Instantiate(gameSettings.PlayerPrefab);
        RemotePlayerBehavior remoteBehavior = new RemotePlayerBehavior(remotePlayer);
        remotePlayer.Setup(remoteBehavior, packet);
        players.Add(remotePlayer.State.Id, remotePlayer);
    }

    private void OnPlayerDisconnect(PlayerDisconnectPacket packet)
    {
        Debug.Log("Disconnect");
    }

    private void OnPlayerState(PlayerStatePacket packet)
    {
        players[packet.Id].UpdateState(packet);
    }
    
    private void Update()
    {
        client.PoolEvents();
    }
}