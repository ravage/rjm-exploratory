using Code.Variables;
using UnityEngine;

public class PlayerState
{
    private PlayerStatePacket statePacket;
    private ThrowPacket throwPacket;
    
    public int Id { get; set; }
    public string Name { get; set; }
    public FloatValue X { get; set; }
    public FloatValue Y { get; set; }
    public FloatValue Horizontal { get; set; }
    public FloatValue Vertical { get; set; }
    public BoolValue Throw { get; set; }

    public PlayerState()
    {
        X = ScriptableObject.CreateInstance<FloatValue>();
        Y = ScriptableObject.CreateInstance<FloatValue>();
        Horizontal = ScriptableObject.CreateInstance<FloatValue>();
        Vertical = ScriptableObject.CreateInstance<FloatValue>();
        Throw = ScriptableObject.CreateInstance<BoolValue>();
        statePacket = new PlayerStatePacket();
        throwPacket = new ThrowPacket();
    }
    public PlayerState(PlayerJoinPacket packet) : this()
    {
        statePacket.Id = Id = packet.Id;
        Name = packet.Name;
        X.Value = packet.X;
        Y.Value = packet.Y;
    }

    public PlayerState Update(PlayerStatePacket packet)
    {
        X.Value = packet.X;
        Y.Value = packet.Y;
        Horizontal.Value = packet.Horizontal;
        Vertical.Value = packet.Vertical;
        Throw.Value = packet.Throw;
        return this;
    }

    public PlayerState Update(ThrowPacket packet)
    {
        Throw.Value = true;
        return this;
    }

    public PlayerStatePacket Packet
    {
        get
        {
            statePacket.X = X.Value;
            statePacket.Y = Y.Value;
            statePacket.Horizontal = Horizontal.Value;
            statePacket.Vertical = Vertical.Value;
            return statePacket;
        }
    }

    public ThrowPacket ThrowPacket
    {
        get
        {
            throwPacket.Id = Id;
            throwPacket.X = X.Value;
            throwPacket.Y = Y.Value;
            return throwPacket;
        }
    }
}
