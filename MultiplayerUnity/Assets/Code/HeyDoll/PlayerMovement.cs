using System;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    public FloatValue Vertical;
    public FloatValue Horizontal;
    public FloatValue X;
    public FloatValue Y;
    private Animator animator;
    private Rigidbody2D rigidbody;
    [SerializeField]
    private float speed;
    private static readonly int horizontalKey = Animator.StringToHash("Horizontal");
    private static readonly int verticalKey = Animator.StringToHash("Vertical");
    
    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Vector3 vector = new Vector3(Horizontal.Value, Vertical.Value, 0);
        vector = vector.normalized * (speed * Time.deltaTime);
        rigidbody.MovePosition(transform.position + vector);
    }

    private void Update()
    {
        Vector3 position = transform.position;
        X.Value = position.x;
        Y.Value = position.y;
        animator.SetFloat(horizontalKey, Horizontal.Value);
        animator.SetFloat(verticalKey, Vertical.Value);
    }
}
