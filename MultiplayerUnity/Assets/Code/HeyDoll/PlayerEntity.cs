using System.Collections;
using TMPro;
using UnityEngine;

public class PlayerEntity : MonoBehaviour
{
    private PlayerState state;
    private PlayerBehavior behavior;
    private Animator animator;
    private Collider2D collider;
    [SerializeField]
    private GameObject kunaiPrefab;
    [SerializeField]
    private Transform kunaiSpawnPosition;
    public void Setup(PlayerBehavior behavior, PlayerJoinPacket packet)
    {
        this.behavior = behavior;
        state = new PlayerState(packet);
        this.behavior.Setup(state);
        animator = GetComponentInChildren<Animator>();
        collider = GetComponent<Collider2D>();
        GetComponentInChildren<TextMeshPro>().text = packet.Name;
        MoveToStartingPosition();
    }

    public PlayerState State => state;
    
    public void UpdateState(PlayerStatePacket packet)
    {
        state.Update(packet);
    }
    
    public void UpdateState(ThrowPacket packet)
    {
        state.Update(packet);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        animator.SetTrigger("Death");
        StartCoroutine(GracePeriod());
    }

    private IEnumerator GracePeriod()
    {
        collider.enabled = false;
        yield return new WaitForSeconds(1f);
        collider.enabled = true;
    }

    private void Update()
    {
        behavior.Process(state);
    }

    private void MoveToStartingPosition()
    {
        transform.position = new Vector3(state.X.Value, state.Y.Value, 0);
    }
}
