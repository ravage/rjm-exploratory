using System;
using TMPro;
using UnityEngine;

[Serializable]
public class GameSettings
{
    [Header("Player Settings")]
    public PlayerEntity PlayerPrefab;
    public TextMeshPro NamePlate;
    [Header("Server Settings")]
    public string Address = "localhost";
    public int Port = 3000;
    public string Key = string.Empty;
}