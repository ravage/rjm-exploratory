using System;
using TMPro;
using UnityEngine;

public class Flipper : MonoBehaviour
{
    public FloatValue Horizontal;
    private Quaternion left;
    private Quaternion right;

    private void Awake()
    {
        right = Quaternion.Euler(0, 0, 0);
        left = Quaternion.Euler(0, 180, 0);
    }

    private void Update()
    {
        if (Horizontal.Value > 0 && transform.rotation == left)
        {
            Flip(0);
        }
        else if (Horizontal.Value < 0 && transform.rotation == right)
        {
            Flip(180);
        }
    }

    private void Flip(int amount)
    {
        transform.rotation = Quaternion.Euler(0, amount, 0);
    }
}