using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Kunai : MonoBehaviour
{
    private Rigidbody2D rigidbody;
    private Collider2D selfCollider;
    public Collider2D Owner { get; set; }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        selfCollider = GetComponent<Collider2D>();
        
    }
    
    private void Start()
    {
        Physics2D.IgnoreCollision(selfCollider, Owner);
        var heading = transform.position - Owner.transform.position;
        heading.y = 0;
        rigidbody.AddRelativeForce(heading.normalized * 10f, ForceMode2D.Impulse);   
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
        //remote.Hit();
    }
}
