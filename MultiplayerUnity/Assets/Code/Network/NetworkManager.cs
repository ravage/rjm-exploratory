using System.Net;
using System.Net.Sockets;
using Code.Interfaces;
using LiteNetLib;
using UnityEngine;

public class NetworkManager : INetEventListener
{
    private NetManager client;
    private GameSettings serverSettings;
    private IPacketHandler packetHandler;
    public NetworkManager(GameSettings serverSettings, IPacketHandler packetHandler)
    {
        this.serverSettings = serverSettings;
        this.packetHandler = packetHandler;
        client = new NetManager(this);
        client.AutoRecycle = true;
        //client.UpdateTime = 15;
    }

    public void Connect()
    {
        client.Start();
        client.Connect(serverSettings.Address, serverSettings.Port, serverSettings.Key);
    }

    public void PoolEvents()
    {
        client.PollEvents();
    }
    
    public void OnPeerConnected(NetPeer peer)
    {
        Debug.Log("OnPeerConnected");
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log("OnPeerDisconnected");
    }

    public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
    {
        throw new System.NotImplementedException();
    }

    public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        packetHandler.Handle(peer, reader, deliveryMethod);
    }

    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
    {
        throw new System.NotImplementedException();
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        
    }

    public void OnConnectionRequest(ConnectionRequest request)
    {
        throw new System.NotImplementedException();
    }

    public void Send<T>(T state) where T : class, new()
    {
        packetHandler.Send(client.FirstPeer, state);
    }
}