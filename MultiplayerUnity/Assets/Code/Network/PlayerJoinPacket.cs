using LiteNetLib.Utils;

public class PlayerJoinPacket : INetSerializable
{
    public int Id { get; set; }
    public string Name { get; set; }
    public float X { get; set; }
    public float Y { get; set; }
    public void Serialize(NetDataWriter writer)
    {
        writer.Put(Id);
        writer.Put(Name);
        writer.Put(X);
        writer.Put(Y);
    }

    public void Deserialize(NetDataReader reader)
    {
        Id = reader.GetInt();
        Name = reader.GetString();
        X = reader.GetFloat();
        Y = reader.GetFloat();
    }
}