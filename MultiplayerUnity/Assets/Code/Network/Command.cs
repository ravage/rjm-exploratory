namespace Code.Network
{
    public enum Command : byte
    {
        SpwanPlayer,
        SpawnRemotePlayer,
        PlayerState
    }
}