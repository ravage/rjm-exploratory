using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Timers;
using Code.Interfaces;
using LiteNetLib;
using LiteNetLib.Utils;
using UnityEngine;

public class PacketHandler : IPacketHandler
{
    public Action<PlayerJoinPacket> OnPlayerJoin;
    public Action<PlayerStatePacket> OnPlayerState;
    public Action<PlayerDisconnectPacket> OnPlayerDisconnect;
    public Action<GameStatePacket> OnGameState;
    public Action<ThrowPacket> OnThrowPacket;
    
    private NetPacketProcessor processor;
    private Dictionary<NetPeer, Queue> queue;
    private Timer sendTimer;

    public PacketHandler()
    {
        processor = new NetPacketProcessor();
        processor.RegisterNestedType(() => new PlayerStatePacket());
        processor.RegisterNestedType(() => new PlayerJoinPacket());
        processor.SubscribeReusable<GameStatePacket>(GameStateHandler);
        processor.SubscribeReusable<PlayerStatePacket>(PlayerStateHandler);
        processor.SubscribeReusable<PlayerJoinPacket>(PlayerJoinPacketHandler);
        processor.SubscribeReusable<PlayerDisconnectPacket>(PlayerDisconnectHandler);
        processor.SubscribeReusable<ThrowPacket>(ThrowPacketHandler);
        
        queue = new Dictionary<NetPeer, Queue>();
        sendTimer = new Timer(100);
        sendTimer.Elapsed += SendTimerOnElapsed;
        sendTimer.Start();
    }

    private void SendTimerOnElapsed(object sender, ElapsedEventArgs e)
    {
        ActualSend();
    }

    private void ThrowPacketHandler(ThrowPacket packet)
    {
       OnThrowPacket?.Invoke(packet);
    }

    public void Handle(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        processor.ReadAllPackets(reader);
    }
    
    public void Send<T>(NetPeer peer, T packet) where T : class, new()
    {
        Queue current;

        if (queue.TryGetValue(peer, out current))
        {
            current.Enqueue(packet);
        }
        else
        {
            current = new Queue();
            current.Enqueue(packet);
            queue.Add(peer, current);
        }
        
        
        //processor.Send(peer, packet, DeliveryMethod.Unreliable);
    }

    private void PlayerDisconnectHandler(PlayerDisconnectPacket packet)
    {
        OnPlayerDisconnect?.Invoke(packet);
    }

    private void PlayerStateHandler(PlayerStatePacket packet)
    {
        OnPlayerState?.Invoke(packet);
    }
    
    private void PlayerJoinPacketHandler(PlayerJoinPacket packet)
    {
       OnPlayerJoin?.Invoke(packet);
    }
    
    private void GameStateHandler(GameStatePacket packet)
    {
        OnGameState?.Invoke(packet);
    }

    private void ActualSend()
    {
        Debug.Log("Sending!");
        foreach (KeyValuePair<NetPeer,Queue> item in queue)
        {
            for (int i = 0; i < item.Value.Count; i++)
            {
                
                object po = item.Value.Dequeue();

                if (po is PlayerStatePacket)
                {
                    processor.Send(item.Key, (po as PlayerStatePacket), DeliveryMethod.Unreliable);    
                }
                else if (po is ThrowPacket)
                {
                    processor.Send(item.Key, (po as ThrowPacket), DeliveryMethod.Unreliable);
                }
            }
        }
    }
}