using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    [SerializeField]
    private Kunai kunaiPrefab;
    [SerializeField]
    private Transform kunaiSpawnPosition;
    
    public void TriggerThrowKunai()
    {
        Kunai kunai = Instantiate(kunaiPrefab, kunaiSpawnPosition.position, transform.rotation);
        kunai.Owner = kunaiSpawnPosition.GetComponentInParent<Collider2D>();
    }
}
