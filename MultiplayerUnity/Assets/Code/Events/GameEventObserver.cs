using UnityEngine;
using UnityEngine.Events;

public class GameEventObserver : MonoBehaviour
{
    public GameEvent Event;
    public UnityEvent Response;

    private void OnEnable() => Event += this;
    private void OnDisable() => Event += this;
    public void Notify() => Response?.Invoke();
}