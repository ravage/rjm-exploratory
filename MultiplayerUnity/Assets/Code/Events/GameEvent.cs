using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Game Event")]
public class GameEvent : ScriptableObject
{
    private IList<GameEventObserver> observers;

    public GameEvent()
    {
        observers = new List<GameEventObserver>();
    }
    
    public void Trigger()
    {
        foreach (GameEventObserver observer in observers)
        {
            observer.Notify();
        }
    }

    public static GameEvent operator +(GameEvent self, GameEventObserver observer)
    {
        self.observers.Add(observer);
        return self;
    }

    public static GameEvent operator -(GameEvent self, GameEventObserver observer)
    {
        self.observers.Remove(observer);
        return self;
    }
}