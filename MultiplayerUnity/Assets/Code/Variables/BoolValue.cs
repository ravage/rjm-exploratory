using UnityEngine;

namespace Code.Variables
{
    [CreateAssetMenu(menuName = "Custom/Bool Value")]
    public class BoolValue : ScriptableObject
    {
        public bool Value { get; set; }
    }
}