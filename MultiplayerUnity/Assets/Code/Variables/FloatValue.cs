using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Float Value")]
public class FloatValue : ScriptableObject
{
    public float Value { get; set; }
}
