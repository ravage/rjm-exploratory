using LiteNetLib;

namespace Code.Interfaces
{
    public interface IPacketHandler
    {
        void Send<T>(NetPeer peer, T packet) where T : class, new();
        void Handle(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod);
    }
}