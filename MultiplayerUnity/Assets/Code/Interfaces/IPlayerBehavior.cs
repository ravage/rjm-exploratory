public interface IPlayerBehavior
{
    void Process(PlayerState state);
    void Update();
    void LateUpdate();
}
