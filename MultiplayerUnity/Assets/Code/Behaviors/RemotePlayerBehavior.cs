using UnityEngine;

public class RemotePlayerBehavior : PlayerBehavior
{
    private Animator animator;
    public RemotePlayerBehavior(PlayerEntity player) : base(player)
    {
        animator = player.GetComponentInChildren<Animator>();
    }

    public override void Process(PlayerState state)
    {
        animator.SetBool("Throw",state.Throw.Value);
        state.Throw.Value = false;
    }
}
