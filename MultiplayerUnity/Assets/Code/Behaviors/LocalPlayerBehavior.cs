using System;
using System.Timers;
using Unity.Collections;
using UnityEngine;

public class LocalPlayerBehavior : PlayerBehavior
{
    private Rigidbody2D rigidbody ;
    private Animator animator;
    public Action OnMotion;
    //public Action OnThrow;
    private static readonly int Throw = Animator.StringToHash("Throw");
    private float lastKunai;
    
    public LocalPlayerBehavior(PlayerEntity player) : base(player)
    {
        rigidbody = player.GetComponent<Rigidbody2D>();
        animator = player.GetComponentInChildren<Animator>();
        lastKunai = Time.time;
    }

    public override void Process(PlayerState state)
    {
        state.Horizontal.Value = Input.GetAxis("Horizontal");
        state.Vertical.Value = Input.GetAxis("Vertical");
        bool throwPressed = Input.GetButton("Fire1");
        
        if (rigidbody.IsAwake())
        {
            OnMotion?.Invoke();                
        }

        if (throwPressed)
        {
            if (Time.time - lastKunai > 1)
            {
                lastKunai = Time.time;
                animator.SetBool(Throw, true);
                OnThrow?.Invoke();
            }
            else
            {
                animator.SetBool(Throw, false);
            }
        }
    }
}