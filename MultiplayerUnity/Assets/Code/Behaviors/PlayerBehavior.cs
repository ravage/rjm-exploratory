using System;
using System.CodeDom;
using UnityEngine;

public abstract class PlayerBehavior
{
    protected PlayerMovement movement;
    protected Flipper flipper;
    public Action OnThrow;

    public PlayerBehavior(PlayerEntity player)
    {
        flipper = player.GetComponentInChildren<Flipper>();
        movement = player.GetComponent<PlayerMovement>();
    }

    public virtual void Setup(PlayerState state)
    {
        HookUpReferences(state);
    }
    
    public virtual void Process(PlayerState state)
    {
    }
    
    private void HookUpReferences(PlayerState state)
    {
        movement.Horizontal = state.Horizontal;
        movement.Vertical = state.Vertical;
        movement.X = state.X;
        movement.Y = state.Y;
        flipper.Horizontal = state.Horizontal;
        //throwBehavior.ThrowRef = state.Throw;
    }
}
